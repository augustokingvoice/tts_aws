#!/bin/bash

echo "Iniciando Instalação"

which python3 > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
    echo "python3 não está instalado, por favor instalar em sua distribuição!"
    exit 1
fi
which pip3 > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
    echo "pip3 não está instalado, por favor instalar!"
    exit 1
fi
wget -q https://gitlab.com/augustokingvoice/tts_aws/-/raw/master/requeriments.txt -O /tmp/requeriments.txt
pip3 install -r /tmp/requeriments.txt > /dev/null
rm -f /tmp/requeriments.txt
wget -q https://gitlab.com/augustokingvoice/tts_aws/-/raw/master/sintetizador.py -O /usr/local/bin/tts_aws
chmod +x /usr/local/bin/tts_aws
echo "Instalação Finalizada"