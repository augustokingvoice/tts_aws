# tts_aws

TTS_AWS é um script em python para utilizar o serviço Polly da Amazon para gerar áudios através de texto.

Após instalar, basta executar ```$tts_aws --help```, que irá exibir como utilizar o utilitário.

## Install

```
wget -qO- https://gitlab.com/augustokingvoice/tts_aws/-/raw/master/install.sh | sudo bash
```

## Config

Para utilizar é necessário criar um arquivo com as credenciais da AWS, ex:
```
[aws]
aws_access_key_id=####
aws_secret_access_key=###
region_name=###
```
As credenciais podem ser obtidas em sua conta na AWS.
O arquivo pode ser salvo em:
```
~/.tts_aws.cfg ou /etc/tts_aws.cfg
```
Também ser passado como parâmetro na execução do script
```
$ tts_aws --conf arquivo_config.cfg --usage
```

## Uninstall

```
sudo rm -f /usr/local/bin/tts_aws
```
