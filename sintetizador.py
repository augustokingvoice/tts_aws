#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import argparse
import configparser
from os.path import isfile,expanduser,join
from datetime import datetime

import boto3

homedir = expanduser("~")
LOCAIS_CREDENCIAS = [
	join(homedir, '.tts_aws.cfg'),
	'/etc/tts_aws.cfg'
]

exemplo_arquivo = """{
	"frases": [
		{
			"nome-audio":"saudacao",
			"frase":"Bem vindo"
		},
		{
			"nome-audio":"sucesso",
			"frase":"Cadastro localizado. "
		},
		{
			"nome-audio":"falha",
			"frase":"Não encontrei o seu cadastro!"
		}
	]
}"""

arquivo_usage_local = '/tmp/usage_polly.json'

def get_argumentos():
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'--frase',
		default=False,
		dest='frase',
		nargs='?',
		help='Frase que será transformada em áudio'
	)
	parser.add_argument(
		'--nome-audio',
		default=False,
		dest='nome_audio',
		nargs='?',
		help='Nome do arquivo que será salvo'
	)
	parser.add_argument(
		'--arquivo',
		default=False,
		dest='arquivo',
		nargs='?',
		help='Local do arquivo JSON com a lista de gravações'
	)
	parser.add_argument(
		'--exemplo-arquivo',
		default=True,
		dest='exemplo',
		nargs='?',
		help='Exibe exemplo de arquivo'
	)
	parser.add_argument(
		'--usage',
		default=True,
		dest='usage',
		nargs='?',
		help='Exibe o total usado no mês, sendo que o limite é de 1000000 carácteres'
	)
	parser.add_argument(
		'--conf',
		default=False,
		dest='conf',
		nargs='?',
		help='Arquivo de configuração'
	)
	parse_args = parser.parse_args()
	return parse_args

def gera_tts(polly=False, nome_audio=False, frase=False):
	response = polly.synthesize_speech(
		VoiceId='Camila',
		Engine='neural',
		SampleRate='8000',
		OutputFormat='mp3', 
		Text = frase
	)
	file = open(f'{nome_audio}.mp3', 'wb')
	file.write(response['AudioStream'].read())
	file.close()

def busca_usage(s3=False):
	s3.download_file('aws-polly-usage', 'usage.json', arquivo_usage_local)
	usage_arquivo = open(arquivo_usage_local, "r")
	usage = json.load(usage_arquivo)
	atual = datetime.now().strftime('%m-%Y')
	if atual in usage.keys():
		return usage[atual]
	else:
		return 0

def checa_usage(usage=0, total=0):
	if usage+total < 1000000:
		return True
	else:
		return False 

def atualiza_usage(s3=False, usage=0, total=0):
	usage_arquivo = open(arquivo_usage_local, "r")
	usage_arquivo = json.load(usage_arquivo)
	atual = datetime.now().strftime('%m-%Y')
	usage_arquivo[atual] = int(usage) + int(total)
	print(f"Total de carácteres gerados no mês: {usage_arquivo[atual]}, Limite: 1000000")
	with open(arquivo_usage_local, 'w') as json_file:
		json.dump(usage_arquivo, json_file)
	s3.upload_file(arquivo_usage_local, 'aws-polly-usage', 'usage.json')
	return True

if __name__ == "__main__":
	modo = None
	config = False
	parametros = get_argumentos()
	if parametros.conf != False:
		if isfile(parametros.conf):
			config = configparser.ConfigParser()
			config.read(parametros.conf)
		else:
			print("Arquivo de configuração invalido")
			exit(1)
	else:
		for arquivo in LOCAIS_CREDENCIAS:
			if isfile(arquivo):
				config = configparser.ConfigParser()
				config.read(arquivo)
				break
		if config == False:
			print("Arquivo de configuração não encontrado")
			exit(1)

	if parametros.frase != False and parametros.nome_audio != False:
		modo = "frase"
	elif parametros.arquivo != False:
		modo = "arquivo"
	elif parametros.exemplo is None:
		print("Gerar um arquivo json conforme exemplo abaixo: \n")
		print(exemplo_arquivo)
	elif parametros.usage is None:
		aws_session = boto3.Session(
			aws_access_key_id=config['aws']['aws_access_key_id'],                     
			aws_secret_access_key=config['aws']['aws_secret_access_key'],
			region_name=config['aws']['region_name']
		)
		total_usage = busca_usage(aws_session.client("s3"))
		print(f"Total de carácteres gerados no mês: {total_usage}, Limite: 1000000")
		exit(0)
	else:
		print("Parametros inválidos!")
		print("Para usar este utilitário para um único audio especificar: --frase e --nome-audio.")
		print("Para usar com uma lista de áudios usar o parametro: --arquivo")
		exit(1)
	aws_session = boto3.Session(
		aws_access_key_id=config['aws']['aws_access_key_id'],                     
		aws_secret_access_key=config['aws']['aws_secret_access_key'],
		region_name=config['aws']['region_name']
	)
	if modo == "frase":
		total_usage = int(busca_usage(aws_session.client("s3")))
		total_frase = len(parametros.nome_audio)
		if checa_usage(usage=total_usage, total=total_frase):
			gera_tts(polly=aws_session.client('polly'),frase=parametros.frase,nome_audio=parametros.nome_audio)
			print("Áudio gerado")
			atualiza_usage(s3=aws_session.client("s3"),usage=total_usage, total=total_frase)
			exit(0)
		else:
			print("Excedido total de carácteres no mês")
			exit(1)

	elif modo == "arquivo":
		if isfile(parametros.arquivo):
			arquivo = open(parametros.arquivo, "r")
			gravacoes = json.load(arquivo)
			total_usage = int(busca_usage(aws_session.client("s3")))
			total_frases = 0
			for gravacao in gravacoes["frases"]:
				total_frases += len(gravacao["frase"])
				if checa_usage(usage=total_usage, total=total_frases):
					gera_tts(polly=aws_session.client('polly'),frase=gravacao["frase"],nome_audio=gravacao["nome-audio"])
					print("Áudio gerado")
				else:
					atualiza_usage(s3=aws_session.client("s3"),usage=total_usage, total=total_frases)
					print("Excedido total de carácteres no mês")
					exit(1)
					break
			atualiza_usage(s3=aws_session.client("s3"),usage=total_usage, total=total_frases)
			print("Fim da lista")
			exit(0)
		else:
			print("Caminho ou arquivo invalido")
			exit(1)